﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestStack.Seleno.Configuration;

namespace JobVacancyWeb.Tests.Acceptance
{
    public static class Host
    {
        public static readonly SelenoHost Instance = new SelenoHost();

        static Host()
        {
            Instance.Run("JobVacancyWeb", 49664);
        }
    }
}
