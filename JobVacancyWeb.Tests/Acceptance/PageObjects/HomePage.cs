﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JobVacancyWeb.Tests.Acceptance.PageObjects
{
    public class HomePage : TestStack.Seleno.PageObjects.Page
    {
        public bool ContainsLinkText(string text)
        {
            var element = this.Find.Element(OpenQA.Selenium.By.LinkText(text));
            return element != null;
        }
    }
}
