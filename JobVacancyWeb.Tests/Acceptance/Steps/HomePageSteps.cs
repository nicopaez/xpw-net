﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NBehave.Spec.NUnit;
using NBehave.Narrator.Framework;
using OpenQA.Selenium;
using NBehave.Narrator.Framework.Hooks;

namespace JobVacancyWeb.Tests.Acceptance
{
    [ActionSteps]
    [Hooks]
    public class HomePageSteps
    {
        private string baseUrl = "http://localhost:49664";
        private IWebDriver driver;

        [Given("I am on the home page")]
        public void VisitHomePage()
        {
           this.driver = new OpenQA.Selenium.Firefox.FirefoxDriver();
           this.driver.Navigate().GoToUrl(baseUrl + "/");
        }

        [Then("I should see Publish a Job")]
        public void ShouldSeePublishAJob()
        {
            var element = this.driver.FindElement(By.LinkText("Publish a Job"));
            element.ShouldNotBeNull();
        }

        [Then("I should see Find a Job")]
        public void ShouldSeeFindAJob()
        {
            var element = this.driver.FindElement(By.LinkText("Find a Job"));
            element.ShouldNotBeNull();
        }
    }
}

